package com.example.vezbe04;

import android.graphics.Bitmap;
import android.widget.ImageView;

public abstract class GameObject {
    protected ImageView image;

    public GameObject(ImageView image){
        this.image = image;
    }

    public float getX()  {
        return this.image.getX();
    }

    public float getY()  {
        return this.image.getY();
    }

    public int getHeight() {
        return this.image.getHeight();
    }

    public int getWidth() {
        return this.image.getWidth();
    }

    public void setX(float x) {
        this.image.setX(x);
    }

    public void setY(float y) {
        this.image.setY(y);
    }
}
