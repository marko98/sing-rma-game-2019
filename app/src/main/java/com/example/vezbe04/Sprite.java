package com.example.vezbe04;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.Log;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * klasa koja opisuje igraca u igri
 */
public class Sprite extends GameObject {
    private FrameLayout frameLayout;

    protected static final int ROW_TOP_TO_BOTTOM = 0;
    protected static final int ROW_RIGHT_TO_LEFT = 1;
    protected static final int ROW_LEFT_TO_RIGHT = 2;
    protected static final int ROW_BOTTOM_TO_TOP = 3;

    protected static int farmePS = 0;
    protected static int FRAME_PER_SEC = 3;

    protected int rowUsing = ROW_LEFT_TO_RIGHT;
    protected int colUsing;

//    private Bitmap[] leftToRights;
//    private Bitmap[] rightToLefts;
//    private Bitmap[] topToBottoms;
//    private Bitmap[] bottomToTops;

    //brzina igraca(pixel/millisecond)
    protected static final float VELOCITY = 0.3f;
    protected static float velocity = VELOCITY;

    protected int movingVectorX = -10;
    protected int movingVectorY = 10; // gravitacija

    protected long lastDrawNanoTime = -1;

//    protected GameSurface gameSurface;

    public Sprite(ImageView image, FrameLayout frameLayout){
        super(image);
        this.frameLayout = frameLayout;
    }

//    public Bitmap[] getMoveBitmaps(){
//        switch (rowUsing) {
//            case ROW_BOTTOM_TO_TOP:
//                return this.bottomToTops;
//            case ROW_LEFT_TO_RIGHT:
//                return this.leftToRights;
//            case ROW_RIGHT_TO_LEFT:
//                return this.rightToLefts;
//            case ROW_TOP_TO_BOTTOM:
//                return this.topToBottoms;
//            default:
//                return null;
//        }
//    }

//    public Bitmap getCurrentMoveBitmap(){
//        Bitmap[] bitmaps = this.getMoveBitmaps();
//        return  bitmaps[this.colUsing];
//    }

    public void update(){
//        Log.d("PROBA", String.valueOf(this.image.getX()));
//        NIJE MI JASNO OVO
        farmePS++;
        if(farmePS%FRAME_PER_SEC==0){
            this.colUsing++;
            farmePS = 0;
        }

//        if(colUsing >= this.colCount){
//            this.colUsing = 0;
//        }

        // trenutno vreme u nanosekundama
        long now = System.nanoTime();

        if(lastDrawNanoTime==-1){
            lastDrawNanoTime = now;
        }

        // pretvaranje vremena iz nanosekundi u milisekunde(1ns = 1000000 ms)
        int deltaTime = (int)((now - lastDrawNanoTime)/1000000);

        // s = v * t
        float distance = VELOCITY * deltaTime;

        // racunanje hipotenuze
        double movingVectorLength = Math.sqrt(movingVectorX*movingVectorX + movingVectorY*movingVectorY);

        // racunanje nove pozicije igraca

        this.image.setX((float) (distance * movingVectorX / movingVectorLength));
        Log.d("PROBA", String.valueOf((distance * movingVectorX / movingVectorLength)));
        Log.d("PROBA", String.valueOf(this.image.getX()));
//        this.image.setY((float) (distance * movingVectorY / movingVectorLength));

        // menjanje putanje usled dodira ivica  ekrana
        if(this.image.getX() < 0){
            this.image.setX(0);
            this.movingVectorX = -this.movingVectorX;
        }
//        else if(this.image.getX() + this.image.getWidth() > this.frameLayout.getWidth()){
//            this.image.setX(this.frameLayout.getWidth() - this.image.getWidth());
//            this.movingVectorX = -this.movingVectorX;
//        }

//        if(this.image.getY() < 0 )  {
//            // otisao u oblake
////            this.y = 0;
////            this.movingVectorY = - this.movingVectorY;
//        } else if(this.image.getY() > this.frameLayout.getHeight() - this.image.getHeight())  {
//            // otisao ispod ekrana
//            this.image.setY(this.frameLayout.getHeight() - this.image.getHeight());
//        }

        // red za renderovanje
//        if(movingVectorX > 0){
//            if(movingVectorY > 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)){
//                this.rowUsing = ROW_TOP_TO_BOTTOM;
//            } else if(movingVectorY < 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)){
//                this.rowUsing = ROW_BOTTOM_TO_TOP;
//            } else {
//                this.rowUsing = ROW_LEFT_TO_RIGHT;
//            }
//        } else {
//            if(movingVectorY > 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)){
//                this.rowUsing = ROW_TOP_TO_BOTTOM;
//            } else if(movingVectorY < 0 && Math.abs(movingVectorX) < Math.abs(movingVectorY)) {
//                this.rowUsing = ROW_BOTTOM_TO_TOP;
//            } else {
//                this.rowUsing = ROW_RIGHT_TO_LEFT;
//            }
//        }


    }

//    public void draw(Canvas canvas){
//        Bitmap bitmap = this.getCurrentMoveBitmap();
//        canvas.drawBitmap(bitmap, this.x, this.y, null);
//        // vreme poslednjeg crtanja
//        this.lastDrawNanoTime = System.nanoTime();
//    }

    public void setMovingVector(int movingVectorX, int movingVectorY){
        this.movingVectorX = movingVectorX;
        this.movingVectorY = movingVectorY;
    }
}
