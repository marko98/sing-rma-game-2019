package com.example.vezbe04;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {
    private GameThread gameThread;
    private Sprite player, enemy;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // postavljanje punog ekrana
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        // uklanjanje akcionog bara
        if (getSupportActionBar() != null) {
            getSupportActionBar().hide();
        }

        setContentView(R.layout.activity_main);

        this.gameThread = new GameThread(this);
        this.gameThread.setRunning(true);
        this.gameThread.start();

        this.frameLayout = findViewById(R.id.frameLayout);

        ImageView playerReference = (ImageView) findViewById(R.id.player);
        player = new Sprite(playerReference, this.frameLayout);
//        Sprite enemy = new Sprite((ImageView) findViewById(R.id.enemy), this.frameLayout);



    }

    @Override
    public void finish() {
        boolean retry = true;
        while (retry){
            try {
                this.gameThread.setRunning(false);
                // Parent thread must wait until the end of GameThread.
                this.gameThread.join();
            } catch (InterruptedException e){
                e.printStackTrace();
            }
            retry = true;
        }

        super.finish();
    }

    public void update(){
//        player.update();
//        Log.d("PROBA", String.valueOf(this.player.getHeight()));
//        Log.d("PROBA", String.valueOf(((ImageView) findViewById(R.id.player)).getHeight()));
//        ((ImageView) this.findViewById(R.id.player)).setX(0);
        player.update();

//        Log.d("PROBA", "radi");
    }
}
