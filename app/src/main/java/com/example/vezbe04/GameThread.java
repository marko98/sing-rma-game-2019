package com.example.vezbe04;

import android.app.Activity;
import android.util.Log;
import android.widget.ImageView;

public class GameThread extends Thread {
    private boolean running;
    private MainActivity activity;

    public GameThread(MainActivity activity){
        this.activity = activity;
    }

    @Override
    public void run(){
        long startTime = System.nanoTime();

        while(running){

            try {
                //yourOperation
                activity.runOnUiThread(new Runnable(){

                    @Override
                    public void run() {
                        activity.update();
                    }});
                super.run();


//                this.activity.update();
            } catch (Exception e) {
                // nista ne radi
            } finally {

            }

            // interval za ponovno iscrtavanje
            long now = System.nanoTime();
            long waitTime = (now - startTime)/1000000;
            if(waitTime < 10){
                waitTime = 10; // ms
            }
            System.out.print(" Wait Time="+ waitTime);

            try {
                // sleep
                this.sleep(waitTime);
            } catch (InterruptedException e){

            }
            startTime = System.nanoTime();
            System.out.print(".");
        }

    }

    public void setRunning(boolean running){
        this.running = running;
    }
}
